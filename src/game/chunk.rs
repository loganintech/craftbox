use glium::*;
use glium::implement_vertex;
use glium::uniforms::Sampler;
use cgmath::*;

use super::block::Block;
use crate::rendering::camera::Camera;

const LAYER_SIZE: usize = 16;

#[derive(Copy, Clone)]
pub struct ChunkVertex {
    pub position: [f32; 3],
    pub tex_coords: [f32; 2],
}
implement_vertex!(ChunkVertex, position, tex_coords);

pub struct Chunk {
    layers: Vec<[[Block; LAYER_SIZE]; LAYER_SIZE]>,
    x: usize,
    z: usize,
    vertex_buffer: vertex::VertexBuffer<f32>,
    vertices: Vec<f32>,
    transform: Matrix4<f64>,
}

// Meant to be Option<&T>, not sure of a safe way to implement something similar



impl Chunk {
    pub fn render(&mut self, frame: &mut Frame, cam: &Camera, tex: &Sampler<'_, glium::Texture2d>, shader: &glium::Program) {
        let projection: [[f64; 4]; 4] = cam.proj_mat().clone().into();
        let camera_view: [[f64; 4]; 4] = cam.proj_mat().clone().into();
        let chunk_transform: [[f64; 4]; 4] = self.transform.clone().into();


        let uniform_buffer = uniform!(texData: tex.clone(), chunkTransform: chunk_transform, cameraView: camera_view, proj: projection);
        let _ = frame.draw(&self.vertex_buffer, &index::NoIndices(index::PrimitiveType::TrianglesList), shader, &uniform_buffer, &Default::default());
    }

    fn find_block_id(&self, x: usize, y: usize, z: usize) -> Option<&Block> {
        self.layers.get(z)?.get(y)?.get(x)
    }

    pub fn gen_mesh(&mut self) {
        for z in 0..self.layers.len() {
            for x in 0..self.layers[z].len() {
                for y in 0..self.layers[z][x].len() {
                    if let Some(block) = self.find_block_id(x, y, z + 1) {

                        let big_chonk = FACES[TOP_FACE].iter().as_slice().chunks(5).collect::<Vec<&[f32]>>();
                        let extended_data = big_chonk.into_iter().map(|xyz: &[f32]| {
                            [xyz[0] + x as f32, xyz[1] + y as f32, xyz[2] + z as f32, xyz[3] + block.atlas_x as f32, xyz[4] + block.atlas_y as f32]
                        }).collect::<Vec<_>>();
                        let flat_data = extended_data.iter().flatten().collect::<Vec<_>>();

                        self.vertices.extend(flat_data);
                    }
                    // if let Some(block) = self.find_block_id(x, y, z.wrapping_sub(1)) {
                    //     self.vertices.extend(FACES[BOTTOM_FACE].iter().as_slice().chunks(3).map(|xyz: &[f32]| {
                    //         [xyz[0] + x as f32, xyz[1] + y as f32, xyz[2] + z as f32, block.atlas_x as f32, block.atlas_y as f32]
                    //     }).collect());
                    // }
                    // if let Some(block) = self.find_block_id(x + 1, y, z) {
                    //     self.vertices.extend(FACES[RIGHT_FACE].iter().as_slice().chunks(3).map(|xyz: &[f32]| {
                    //         [xyz[0] + x as f32, xyz[1] + y as f32, xyz[2] + z as f32, block.atlas_x as f32, block.atlas_y as f32]
                    //     }).collect());
                    // }
                    // //This will wrap over to
                    // if let Some(block) = self.find_block_id(x.wrapping_sub(1), y, z) {
                    //     self.vertices.extend(FACES[LEFT_FACE].iter().as_slice().chunks(3).map(|xyz: &[f32]| {
                    //         [xyz[0] + x as f32, xyz[1] + y as f32, xyz[2] + z as f32, block.atlas_x as f32, block.atlas_y as f32]
                    //     }).collect());
                    // }
                    // if let Some(block) = self.find_block_id(x, y + 1, z) {
                    //     self.vertices.extend(FACES[BACK_FACE].iter().as_slice().chunks(3).map(|xyz: &[f32]| {
                    //         [xyz[0] + x as f32, xyz[1] + y as f32, xyz[2] + z as f32, block.atlas_x as f32, block.atlas_y as f32]
                    //     }).collect());
                    // }
                    // if let Some(block) = self.find_block_id(x, y.wrapping_sub(1), z) {
                    //     self.vertices.extend(FACES[FRONT_FACE].iter().as_slice().chunks(3).map(|xyz: &[f32]| {
                    //         [xyz[0] + x as f32, xyz[1] + y as f32, xyz[2] + z as f32, block.atlas_x as f32, block.atlas_y as f32]
                    //     }).collect());
                    // }
                }
            }
        }

        if self.vertices.len() % 5 == 0 {
            // Had some old gl code, removed since we're porting to glium instead
            // Keeping this here as a reminder in case we were using it for something
        }
    }
}

const FRONT_FACE: usize = 0;
const BACK_FACE: usize = 1;
const RIGHT_FACE: usize = 2;
const LEFT_FACE: usize = 3;
const BOTTOM_FACE: usize = 4;
const TOP_FACE: usize = 5;

const FACES: [[f32; 30]; 6] = [
    [
        0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0,
    ],
    [
        1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
    ],
    [
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0,
    ],
    [
        1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0,
    ],
    [
        0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0,
    ],
    [
        1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0,
    ],
];
