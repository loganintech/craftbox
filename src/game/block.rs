///! A trait to mark things as destructable
// trait Destructable {
//     fn durability() -> u64;
// }



// macro_rules! block_ids {
//     ($($assignment:tt => $btype:ident),*) => {

//         ///! An enumeration of
//         #[derive(Eq, PartialEq, Debug, Copy, Clone)]        
//         pub enum BlockID {
//             $($assignment)*,
//         }

//         impl From<&str for BlockID {
//             fn from(val: &str) -> Self {
//                 match val {
//                     $($assignment => $btype)*
//                 }
//             }
//         }
//     }
// }

// block_ids!("air" => Air, "stone.smooth" => SmoothStone, "stone.cobble" => CobbleStone);



pub enum BlockID {
    UNKNOWN,
    Air,
    SmoothStone,
    CobbleStone,
    Dirt,
    Grass,
}

impl From<&str> for BlockID {
    fn from(val: &str) -> Self {
        match val {
            "air" => BlockID::Air,
            "stone.smooth" => BlockID::SmoothStone,
            "stone.cobble" => BlockID::CobbleStone,
            "dirt.grass" => BlockID::Grass,
            "dirt.dirt" => BlockID::Dirt,
            _ => BlockID::UNKNOWN,
        }
    }
}

///! A block
pub struct Block {
    pub id: BlockID,
    pub atlas_x: usize,
    pub atlas_y: usize,
}

impl Block {
    pub fn new<T: Into<BlockID>>(id: T, atlas_x: usize, atlas_y: usize) -> Block {
        Block {
            id: id.into(),
            atlas_x,
            atlas_y,
        }
    }
}

//pub struct BlockDictionary {
//    blocks: HashMap<Block>,
//    texture: Option<usize>,
//}

//impl BlockDictionary {
//    pub fn new() -> BlockDictionary {
//        BlockDictionary {
//            blocks: Vec::new(),
//            texture: None,
//        }
//    }
//
//    pub fn push(&mut self, new_block: Block) {
//
//    }
//}

///! A block that has physics interaction
pub struct PhysicsBlock {}
