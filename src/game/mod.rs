//pub mod block;
pub mod chunk;
pub mod block;

use chunk::{Chunk, ChunkVertex};
use glium::*;
use glium::texture::RawImage2d;
use glium::uniforms::Sampler;
use std::collections::LinkedList;

use std::time::SystemTime;


//Constants
const SCREEN_WIDTH: u32 = 800;
const SCREEN_HEIGHT: u32 = 600;

enum InitError {
    ProblemWithWorld,
}

pub struct Game {
    map: LinkedList<Chunk>,  // IMO best method of storing chunks
}

static mut CHUNK_SHADER: Option<Program> = None;


fn initialize() -> (glutin::EventsLoop, glium::Display, Texture2d) {
    let events_loop = glutin::EventsLoop::new();
    let window = glutin::WindowBuilder::new()
        .with_title("Craftbox")
        .with_dimensions(glutin::dpi::LogicalSize::new(
            SCREEN_WIDTH as f64,
            SCREEN_HEIGHT as f64,
        ));
    let context = glutin::ContextBuilder::new().with_vsync(true);
    let display = glium::Display::new(window, context, &events_loop).unwrap();


    let img = image::load_from_memory(include_bytes!("../rendering/textures/cobble.png")).unwrap().to_rgba();
    let tex_dim: (u32, u32) = (img.width(), img.height());
    let tex = Texture2d::new(&display, RawImage2d::from_raw_rgba(img.into_vec(), tex_dim)).unwrap();

    (events_loop, display, tex)
}

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    let (mut events_loop, gl_window, tex) = initialize();

    let mut running = true;
    let mut frame = gl_window.draw();

    let vertices = &[
        ChunkVertex {
            position: [-0.5, -0.5, 0.0],
            tex_coords: [-0.5, -0.5],
        },
        ChunkVertex {
            position: [0.5, -0.5, 0.0],
            tex_coords: [-0.5, 0.5],
        },
        ChunkVertex {
            position: [-0.5, 0.5, 0.0],
            tex_coords: [-0.5, 0.5],
        },
        ChunkVertex {
            position: [0.5, 0.5, 0.0],
            tex_coords: [0.5, 0.5],
        },
    ];

    let vertex_buffer = vertex::VertexBuffer::new(&gl_window, vertices).unwrap();
    let chunk_texture = uniforms::Sampler::new(&tex);
    let uniform_buffer = uniform!(texData: chunk_texture.magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest));
    let shader = glium::Program::from_source(&gl_window, crate::rendering::shaders::CHUNK_VERTEX_SHADER_SOURCE, crate::rendering::shaders::CHUNK_FRAG_SHADER_SOURCE, None).unwrap();

    while running {
        events_loop.poll_events(|event| {
            if let glutin::Event::WindowEvent { event, .. } = event {
                match event {
                    glutin::WindowEvent::CloseRequested => running = false,
                    //Resized(size) => gl_window.resize(size.to_physical(1_f64)),
                    _ => (),
                }
            }
        });
        frame.clear_color(0.0, 0.0, 0.0, 1.0);

        frame.draw(&vertex_buffer, &index::NoIndices(index::PrimitiveType::TriangleStrip), &shader, &uniform_buffer, &Default::default()).unwrap();
        gl_window.swap_buffers().unwrap();
    }
    frame.finish().unwrap();

    Ok(())
}

fn handle_input() {}

fn draw_game() {}
