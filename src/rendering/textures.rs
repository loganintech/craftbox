use image::GenericImageView;

// https://github.com/PistonDevelopers/image#6-examples
fn load_texture(path: &str) -> image::DynamicImage {
    image::open(path).unwrap()
}

const LEVEL_OF_DETAIL: gl::types::GLint = 0;

///! Uses gl::TexImage2D to load 2D textures into the graphics card
///! https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage2D.xhtml
pub fn load_cobble_texture() {
    let image = load_texture("textures/cobble.png");
    let image = image.to_rgba();

    unsafe {
        gl::TexImage2D(
            gl::TEXTURE_2D,
            LEVEL_OF_DETAIL,
            gl::RGBA as i32,
            image.width() as i32,
            image.height() as i32,
            0,
            gl::RGBA,
            gl::UNSIGNED_BYTE,
            image.into_raw().as_ptr() as *const std::ffi::c_void,
        );
    }
}
