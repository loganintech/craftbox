extern crate cgmath;
use cgmath::*;

pub struct Camera {
	projection: cgmath::Matrix4<f64>,
	position: cgmath::Point3<f64>,
	yaw: f64, pitch: f64,
}

impl Camera {
	pub fn new(fov: f64, width: usize, height: usize) -> Camera {
		Camera {
			projection: cgmath::perspective(Deg(fov), (width/height) as f64, 0.1, 1000.0),
			position: cgmath::Point3{x: 0.0, y: 0.0, z: 0.0},
			yaw: 0.0, pitch: 0.0,
		}
	}

	pub fn proj_mat(&self) -> &cgmath::Matrix4<f64> {
		&self.projection
	}

	pub fn view_mat(&self) -> Matrix4<f64> {
		cgmath::Matrix4::look_at_dir(
			self.position, 
			cgmath::Vector3 {
				x: self.yaw.cos() * self.pitch.cos(),
				y: self.pitch.sin(),
				z: self.yaw.sin() * self.pitch.cos()
			}, 
			cgmath::Vector3{x: 0.0, y: 1.0, z: 0.0}
		)
	}

	pub fn set_pos(&mut self, x: f64, y: f64, z: f64) {
		self.position.x = x;
		self.position.y = y;
		self.position.z = z;
	}

	pub fn add_pos(&mut self, x: f64, y: f64, z: f64) {
		self.position.x += x;
		self.position.y += y;
		self.position.z += z;
	}
	
	pub fn rotate(&mut self, yaw: f64, pitch: f64) {
		self.yaw += yaw;
		self.pitch += pitch;
	}

	pub fn set_rotation(&mut self, yaw: f64, pitch: f64) {
		self.yaw = yaw;
		self.pitch = pitch;
	}
}