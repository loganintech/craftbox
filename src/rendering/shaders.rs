pub const CHUNK_FRAG_SHADER_SOURCE: &str = r#"
#version 450 core

uniform sampler2D texData;
in vec2 TexCoord;
out vec4 FragColor;
void main()
{
    FragColor = texture(texData, TexCoord);
}"#;

pub const CHUNK_VERTEX_SHADER_SOURCE: &str = r#"
#version 450 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 tex_coords;

uniform mat4 chunkTransform;
uniform mat4 cameraView;
uniform mat4 proj;

out vec2 TexCoord;

void main()
{
    TexCoord = tex_coords;
    //gl_Position = proj * cameraView * chunkTransform * vec4(position, 1.0f);
    gl_Position = vec4(position, 1.0f);
}"#;
