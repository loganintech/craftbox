#![allow(dead_code)]

mod game;
mod rendering;

use std::time::SystemTime;

//External Includes
fn main() -> Result<(), Box<dyn std::error::Error + 'static>> {
    println!(
        "Starting {} seconds after UNIX_EPOCH.",
        SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)?
            .as_secs()
    );

    if let Err(e) = game::run() {
        eprintln!("Error occured running the game: {}", e);
        std::process::exit(1);
    };

    Ok(())
}
